# Thai translation of Menu sections
# Copyright (C) 2006-2007 Software in the Public Interest, Inc.
# This file is distributed under the same license as the menu package.
# Theppitak Karoonboonyanan <thep@linux.thai.net>, 2006-2007.
#
msgid ""
msgstr ""
"Project-Id-Version: menu-section 2.1.9-3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-01-21 18:53-0500\n"
"PO-Revision-Date: 2007-07-18 13:57+0700\n"
"Last-Translator: Theppitak Karoonboonyanan <thep@linux.thai.net>\n"
"Language-Team: Thai <thai-l10n@googlegroups.com>\n"
"Language: th\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: debian/desktop-files/ActionGames.directory.desktop.in:3
msgid "Action"
msgstr "แอ็คชั่น"

#: debian/desktop-files/ActionGames.directory.desktop.in:4
msgid "Action games"
msgstr "เกมแอ็คชั่น"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/ActionGames.directory.desktop.in:6
msgid "weather-storm"
msgstr ""

#: debian/desktop-files/Settings-System.directory.desktop.in:4
msgid "Administration"
msgstr "ดูแลระบบ"

#: debian/desktop-files/Settings-System.directory.desktop.in:5
msgid "Change system-wide settings (affects all users)"
msgstr "การตั้งค่าที่มีผลทั้งระบบ (มีผลต่อผู้ใช้ทุกคน)"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/Settings-System.directory.desktop.in:7
#, fuzzy
msgid "preferences-system"
msgstr "ปรับแต่งพื้นโต๊ะ"

#: debian/desktop-files/KidsGames.directory.desktop.in:3
msgid "Kids"
msgstr "เด็ก"

#: debian/desktop-files/KidsGames.directory.desktop.in:4
msgid "Games for kids"
msgstr "เกมสำหรับเด็ก"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/KidsGames.directory.desktop.in:6
msgid "gnome-amusements"
msgstr ""

#: debian/desktop-files/CardGames.directory.desktop.in:3
msgid "Cards"
msgstr "ไพ่"

#: debian/desktop-files/CardGames.directory.desktop.in:4
msgid "Card games"
msgstr "เกมไพ่"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/CardGames.directory.desktop.in:6
msgid "gnome-aisleriot"
msgstr ""

#: debian/desktop-files/Debian.directory.desktop.in:3
msgid "Debian"
msgstr "เดเบียน"

#: debian/desktop-files/Debian.directory.desktop.in:4
msgid "The Debian menu"
msgstr "เมนูเดเบียน"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/Debian.directory.desktop.in:6
msgid "debian-logo"
msgstr ""

#: debian/desktop-files/SimulationGames.directory.desktop.in:3
msgid "Simulation"
msgstr "จำลอง"

#: debian/desktop-files/SimulationGames.directory.desktop.in:4
msgid "Simulation games"
msgstr "เกมจำลองสถานการณ์"

#: debian/desktop-files/BoardGames.directory.desktop.in:3
msgid "Board"
msgstr "กระดาน"

#: debian/desktop-files/BoardGames.directory.desktop.in:4
msgid "Board games"
msgstr "เกมหมากกระดาน"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/BoardGames.directory.desktop.in:6
msgid "desktop"
msgstr ""

#: debian/desktop-files/GnomeScience.directory.desktop.in:3
msgid "Science"
msgstr "วิทยาศาสตร์"

#: debian/desktop-files/GnomeScience.directory.desktop.in:4
#, fuzzy
msgid "Scientific applications"
msgstr "โปรแกรม"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/GnomeScience.directory.desktop.in:6
msgid "applications-science"
msgstr ""

#: debian/desktop-files/StrategyGames.directory.desktop.in:3
msgid "Strategy"
msgstr "วางแผน"

#: debian/desktop-files/StrategyGames.directory.desktop.in:4
msgid "Strategy games"
msgstr "เกมวางแผน"

#: debian/desktop-files/ArcadeGames.directory.desktop.in:3
msgid "Arcade"
msgstr "โต้ตอบ"

#: debian/desktop-files/ArcadeGames.directory.desktop.in:4
msgid "Arcade style games"
msgstr "เกมโต้ตอบ"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/ArcadeGames.directory.desktop.in:6
msgid "gnome-joystick"
msgstr ""

#: debian/desktop-files/Settings.directory.desktop.in:3
msgid "Preferences"
msgstr "ปรับแต่งพื้นโต๊ะ"

#: debian/desktop-files/Settings.directory.desktop.in:4
msgid "Personal preferences"
msgstr "ปรับแต่งพื้นโต๊ะส่วนตัว"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/Settings.directory.desktop.in:6
#, fuzzy
msgid "preferences-desktop"
msgstr "ปรับแต่งพื้นโต๊ะ"

#: debian/desktop-files/BlocksGames.directory.desktop.in:3
msgid "Falling blocks"
msgstr "บล็อคหล่น"

#: debian/desktop-files/BlocksGames.directory.desktop.in:4
msgid "Falling blocks games"
msgstr "เกมบล็อคหล่น"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/BlocksGames.directory.desktop.in:6
msgid "quadrapassel"
msgstr ""

#: debian/desktop-files/SportsGames.directory.desktop.in:3
msgid "Sports"
msgstr "กีฬา"

#: debian/desktop-files/SportsGames.directory.desktop.in:4
msgid "Sports games"
msgstr "เกมกีฬา"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/SportsGames.directory.desktop.in:6
msgid "trophy-gold"
msgstr ""

#: debian/desktop-files/LogicGames.directory.desktop.in:3
msgid "Logic"
msgstr "ตรรกะ"

#: debian/desktop-files/LogicGames.directory.desktop.in:4
msgid "Logic and puzzle games"
msgstr "เกมตรรกะและปริศนา"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/LogicGames.directory.desktop.in:6
msgid "system-run"
msgstr ""

#: debian/desktop-files/AdventureGames.directory.desktop.in:3
msgid "Adventure"
msgstr "ผจญภัย"

#: debian/desktop-files/AdventureGames.directory.desktop.in:4
msgid "Adventure style games"
msgstr "เกมผจญภัย"

#: debian/desktop-files/RolePlayingGames.directory.desktop.in:3
msgid "Role playing"
msgstr "เล่นบทบาท"

#: debian/desktop-files/RolePlayingGames.directory.desktop.in:4
msgid "Role playing games"
msgstr "เกมเล่นบทบาท"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/RolePlayingGames.directory.desktop.in:6
msgid "avatar-default"
msgstr ""
