# translation of wo.po to Wolof
# Menu section translation
# Copyright (C) 2003
# This file is distributed under the same license as the menu package.
#
# Bill Allombert <ballombe@debian.org>, 2003.
# Mouhamadou Mamoune Mbacke <mouhamadoumamoune@gmail.com>, 2006, 2007.
msgid ""
msgstr ""
"Project-Id-Version: wo\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-01-21 18:53-0500\n"
"PO-Revision-Date: 2007-07-19 00:09+0200\n"
"Last-Translator: Mouhamadou Mamoune Mbacke <mouhamadoumamoune@gmail.com>\n"
"Language-Team: Wolof\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#: debian/desktop-files/ActionGames.directory.desktop.in:3
msgid "Action"
msgstr "Aksioŋ"

#: debian/desktop-files/ActionGames.directory.desktop.in:4
msgid "Action games"
msgstr "Fo yu aksioŋ"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/ActionGames.directory.desktop.in:6
msgid "weather-storm"
msgstr ""

#: debian/desktop-files/Settings-System.directory.desktop.in:4
msgid "Administration"
msgstr ""

#: debian/desktop-files/Settings-System.directory.desktop.in:5
msgid "Change system-wide settings (affects all users)"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/Settings-System.directory.desktop.in:7
msgid "preferences-system"
msgstr ""

#: debian/desktop-files/KidsGames.directory.desktop.in:3
msgid "Kids"
msgstr "Gone"

#: debian/desktop-files/KidsGames.directory.desktop.in:4
msgid "Games for kids"
msgstr "Fo yuy gone"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/KidsGames.directory.desktop.in:6
msgid "gnome-amusements"
msgstr ""

#: debian/desktop-files/CardGames.directory.desktop.in:3
msgid "Cards"
msgstr "Ay kaart"

#: debian/desktop-files/CardGames.directory.desktop.in:4
msgid "Card games"
msgstr "Fo yu kaart"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/CardGames.directory.desktop.in:6
msgid "gnome-aisleriot"
msgstr ""

#: debian/desktop-files/Debian.directory.desktop.in:3
msgid "Debian"
msgstr "Debian"

#: debian/desktop-files/Debian.directory.desktop.in:4
msgid "The Debian menu"
msgstr "Menu bu Debian"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/Debian.directory.desktop.in:6
msgid "debian-logo"
msgstr ""

#: debian/desktop-files/SimulationGames.directory.desktop.in:3
msgid "Simulation"
msgstr "Simulaasioŋ"

#: debian/desktop-files/SimulationGames.directory.desktop.in:4
msgid "Simulation games"
msgstr "Fo yu simulaasioŋ"

#: debian/desktop-files/BoardGames.directory.desktop.in:3
msgid "Board"
msgstr "Taabal"

#: debian/desktop-files/BoardGames.directory.desktop.in:4
msgid "Board games"
msgstr "Fo yu taabal"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/BoardGames.directory.desktop.in:6
msgid "desktop"
msgstr ""

#: debian/desktop-files/GnomeScience.directory.desktop.in:3
msgid "Science"
msgstr ""

#: debian/desktop-files/GnomeScience.directory.desktop.in:4
msgid "Scientific applications"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/GnomeScience.directory.desktop.in:6
msgid "applications-science"
msgstr ""

#: debian/desktop-files/StrategyGames.directory.desktop.in:3
msgid "Strategy"
msgstr "Estraateji"

#: debian/desktop-files/StrategyGames.directory.desktop.in:4
msgid "Strategy games"
msgstr "Fo yu estraateji"

#: debian/desktop-files/ArcadeGames.directory.desktop.in:3
msgid "Arcade"
msgstr "Arkaad"

#: debian/desktop-files/ArcadeGames.directory.desktop.in:4
msgid "Arcade style games"
msgstr "Fo yu ame estil u arkaad"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/ArcadeGames.directory.desktop.in:6
msgid "gnome-joystick"
msgstr ""

#: debian/desktop-files/Settings.directory.desktop.in:3
msgid "Preferences"
msgstr ""

#: debian/desktop-files/Settings.directory.desktop.in:4
msgid "Personal preferences"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/Settings.directory.desktop.in:6
msgid "preferences-desktop"
msgstr ""

#: debian/desktop-files/BlocksGames.directory.desktop.in:3
msgid "Falling blocks"
msgstr "Blok yuy daanu"

#: debian/desktop-files/BlocksGames.directory.desktop.in:4
msgid "Falling blocks games"
msgstr "Fo yu blok yuy daanu"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/BlocksGames.directory.desktop.in:6
msgid "quadrapassel"
msgstr ""

#: debian/desktop-files/SportsGames.directory.desktop.in:3
msgid "Sports"
msgstr "Espoor"

#: debian/desktop-files/SportsGames.directory.desktop.in:4
msgid "Sports games"
msgstr "Fo yu espoor"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/SportsGames.directory.desktop.in:6
msgid "trophy-gold"
msgstr ""

#: debian/desktop-files/LogicGames.directory.desktop.in:3
msgid "Logic"
msgstr "Logik"

#: debian/desktop-files/LogicGames.directory.desktop.in:4
msgid "Logic and puzzle games"
msgstr "Fo yu logik ak yu puzzle"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/LogicGames.directory.desktop.in:6
msgid "system-run"
msgstr ""

#: debian/desktop-files/AdventureGames.directory.desktop.in:3
msgid "Adventure"
msgstr "Awantiir"

#: debian/desktop-files/AdventureGames.directory.desktop.in:4
msgid "Adventure style games"
msgstr "Fo yu ame esli u Awantiir"

#: debian/desktop-files/RolePlayingGames.directory.desktop.in:3
msgid "Role playing"
msgstr "Def ay rol"

#: debian/desktop-files/RolePlayingGames.directory.desktop.in:4
msgid "Role playing games"
msgstr "Fo yu def ay rol"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/RolePlayingGames.directory.desktop.in:6
msgid "avatar-default"
msgstr ""
