# Menu section translation
# Copyright (C) 2003
# This file is distributed under the same license as the menu package.
# Bill Allombert <ballombe@debian.org>, 2003.
#
msgid ""
msgstr ""
"Project-Id-Version: menu-section 2.1.7-2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-01-21 18:53-0500\n"
"PO-Revision-Date: 2004-05-23 22:07+0200\n"
"Last-Translator: Stefano Canepa <sc@linux.it>\n"
"Language-Team: Italian <tp@list.linux.it>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"msgid \"\"\n"
"msgstr \"\"\n"

#: debian/desktop-files/ActionGames.directory.desktop.in:3
#, fuzzy
msgid "Action"
msgstr "Apprendimento"

#: debian/desktop-files/ActionGames.directory.desktop.in:4
#, fuzzy
msgid "Action games"
msgstr "Apprendimento"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/ActionGames.directory.desktop.in:6
msgid "weather-storm"
msgstr ""

#: debian/desktop-files/Settings-System.directory.desktop.in:4
msgid "Administration"
msgstr "Amministrazione"

#: debian/desktop-files/Settings-System.directory.desktop.in:5
msgid "Change system-wide settings (affects all users)"
msgstr "Modifica impostazioni di sistema (influenzano tutti gli utenti)"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/Settings-System.directory.desktop.in:7
#, fuzzy
msgid "preferences-system"
msgstr "Preferenze"

#: debian/desktop-files/KidsGames.directory.desktop.in:3
msgid "Kids"
msgstr ""

#: debian/desktop-files/KidsGames.directory.desktop.in:4
msgid "Games for kids"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/KidsGames.directory.desktop.in:6
msgid "gnome-amusements"
msgstr ""

#: debian/desktop-files/CardGames.directory.desktop.in:3
#, fuzzy
msgid "Cards"
msgstr "Carte"

#: debian/desktop-files/CardGames.directory.desktop.in:4
msgid "Card games"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/CardGames.directory.desktop.in:6
msgid "gnome-aisleriot"
msgstr ""

#: debian/desktop-files/Debian.directory.desktop.in:3
msgid "Debian"
msgstr ""

#: debian/desktop-files/Debian.directory.desktop.in:4
msgid "The Debian menu"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/Debian.directory.desktop.in:6
msgid "debian-logo"
msgstr ""

#: debian/desktop-files/SimulationGames.directory.desktop.in:3
msgid "Simulation"
msgstr "Simulazione"

#: debian/desktop-files/SimulationGames.directory.desktop.in:4
#, fuzzy
msgid "Simulation games"
msgstr "Simulazione"

#: debian/desktop-files/BoardGames.directory.desktop.in:3
msgid "Board"
msgstr "Da tavolo"

#: debian/desktop-files/BoardGames.directory.desktop.in:4
#, fuzzy
msgid "Board games"
msgstr "Da tavolo"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/BoardGames.directory.desktop.in:6
msgid "desktop"
msgstr ""

#: debian/desktop-files/GnomeScience.directory.desktop.in:3
msgid "Science"
msgstr "Scienza"

#: debian/desktop-files/GnomeScience.directory.desktop.in:4
msgid "Scientific applications"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/GnomeScience.directory.desktop.in:6
msgid "applications-science"
msgstr ""

#: debian/desktop-files/StrategyGames.directory.desktop.in:3
msgid "Strategy"
msgstr "Strategia"

#: debian/desktop-files/StrategyGames.directory.desktop.in:4
#, fuzzy
msgid "Strategy games"
msgstr "Strategia"

#: debian/desktop-files/ArcadeGames.directory.desktop.in:3
msgid "Arcade"
msgstr "Arcade"

#: debian/desktop-files/ArcadeGames.directory.desktop.in:4
msgid "Arcade style games"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/ArcadeGames.directory.desktop.in:6
msgid "gnome-joystick"
msgstr ""

#: debian/desktop-files/Settings.directory.desktop.in:3
msgid "Preferences"
msgstr "Preferenze"

#: debian/desktop-files/Settings.directory.desktop.in:4
msgid "Personal preferences"
msgstr "Preferenze personali"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/Settings.directory.desktop.in:6
#, fuzzy
msgid "preferences-desktop"
msgstr "Preferenze"

#: debian/desktop-files/BlocksGames.directory.desktop.in:3
msgid "Falling blocks"
msgstr ""

#: debian/desktop-files/BlocksGames.directory.desktop.in:4
msgid "Falling blocks games"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/BlocksGames.directory.desktop.in:6
msgid "quadrapassel"
msgstr ""

#: debian/desktop-files/SportsGames.directory.desktop.in:3
msgid "Sports"
msgstr "Sport"

#: debian/desktop-files/SportsGames.directory.desktop.in:4
#, fuzzy
msgid "Sports games"
msgstr "Sport"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/SportsGames.directory.desktop.in:6
msgid "trophy-gold"
msgstr ""

#: debian/desktop-files/LogicGames.directory.desktop.in:3
msgid "Logic"
msgstr ""

#: debian/desktop-files/LogicGames.directory.desktop.in:4
msgid "Logic and puzzle games"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/LogicGames.directory.desktop.in:6
msgid "system-run"
msgstr ""

#: debian/desktop-files/AdventureGames.directory.desktop.in:3
msgid "Adventure"
msgstr "Avventura"

#: debian/desktop-files/AdventureGames.directory.desktop.in:4
#, fuzzy
msgid "Adventure style games"
msgstr "Avventura"

#: debian/desktop-files/RolePlayingGames.directory.desktop.in:3
msgid "Role playing"
msgstr ""

#: debian/desktop-files/RolePlayingGames.directory.desktop.in:4
msgid "Role playing games"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: debian/desktop-files/RolePlayingGames.directory.desktop.in:6
msgid "avatar-default"
msgstr ""
